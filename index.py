import discord
import asyncpg
import tracemalloc as tm
import asyncio
from secret import token, password
from discord.ext import commands

tm.start()
intents = discord.Intents.all()
extensions = ["cogs.fun", "cogs.help", "cogs.mod", "cogs.info", "cogs.logs", "cogs.suggestions", "cogs.developer",
              "cogs.config", "cogs.eco"]


async def get_prefix(bot, message):
    connection = await asyncpg.connect(database="MEE7Data", user="postgres", password=password)
    guild = await connection.fetchrow("SELECT * FROM guilds WHERE guild_id = $1", message.guild.id)
    await connection.close()
    if not guild:
        return ";"
    return guild['prefix']


class MEE7(commands.AutoShardedBot):
    def __init__(self):
        super().__init__(command_prefix=get_prefix, case_insensitive=True, help_command=None, shard_count=1, intents=intents)
        loop = asyncio.get_event_loop()
        self.db = loop.run_until_complete(asyncpg.create_pool(database='MEE7Data', user='postgres', password=password))
        self.load_extension("jishaku")
        jsk = self.get_command("jishaku")
        jsk.hidden = True
        for extension in extensions:
            self.load_extension(extension)

    async def on_command_error(self, ctx, error):
        if isinstance(error, commands.CommandNotFound):
            pass
        elif isinstance(error, commands.MissingPermissions):
            await ctx.send(f"You are missing permissions. You require `{error.missing_perms}` to use this")
        elif isinstance(error, commands.BadArgument):
            await ctx.send(
                f"Your arguments are not correct. Please view help on this command using `;help {ctx.invoked_with}` to see how to use it")
            print(error)
        elif isinstance(error, commands.MissingRequiredArgument):
            await ctx.send(f"You missed a required argument. Make sure to add `{error.param}` when using this command")
        elif isinstance(error, commands.NotOwner):
            await ctx.send("Only developers can use this command :)")
        elif isinstance(error, commands.BotMissingPermissions):
            await ctx.send(
                f"I am missing permissions to perform this operation. I am missing {error.missing_perms} permissions")
        elif isinstance(error, commands.NSFWChannelRequired):
            await ctx.send(
                f"Woah, I'm not getting banned anytime soon! You need to be in an NSFW channel to execute this command!")
        elif isinstance(error, commands.ExtensionAlreadyLoaded):
            await ctx.send("I already loaded that cog. No need to load it again")
        elif isinstance(error, commands.ExtensionNotFound):
            await ctx.send(f"Hmmm I can't seem to find {error.name}")
        else:
            await ctx.send(f"{error}")

    async def on_ready(self):
        print(f"MEE7 is up and running at {round(self.latency * 1000)}ms")
        await self.change_presence(status=discord.Status.dnd,
                                   activity=discord.Activity(type=discord.ActivityType.listening, name=";help"))

    async def close(self):
        await self.db.close()
        print("DB Closed")
        await super().close()


if __name__ == "__main__":
    MEE7().run(token)
