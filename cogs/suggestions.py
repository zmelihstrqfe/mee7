import discord
from secret import password
import typing
from discord.ext import commands


class Suggestions(commands.Cog):
    """Server suggestion system module"""
    def __init__(self, bot):
        self.bot = bot
        self._last_member = None

    @commands.group(aliases=["suggestion"])
    async def suggest(self, ctx):
        """All the suggestion related commands"""
        if ctx.invoked_subcommand is None:
            await ctx.send("Please use ;help suggestion for more help on this command")

    @suggest.command()
    async def add(self, ctx, suggestion: typing.Optional[str]):
        if suggestion is None:
            await ctx.send("Please use ;help suggest to find out more info on this command")
        channel = await self.bot.db.fetchrow("SELECT * FROM schannel WHERE guild_id = $1", ctx.guild.id)
        schannel = self.bot.get_channel(int(channel['channel_id']))
        snum = 1
        async for message in schannel.history():
            if message.embeds:
                num = len(message.embeds)
                snum += num
        e = discord.Embed(title=f"Suggestion #{snum}!", description=f"{suggestion}", color=0xff0000,
                          timestamp=ctx.message.created_at)
        e.set_footer(text=f"{ctx.author.name}#{ctx.author.discriminator}", icon_url=ctx.author.avatar_url_as(format='png'))
        msg = await schannel.send(embed=e)
        await msg.add_reaction(u"\U0001F44D")
        await msg.add_reaction(u"\U0001F44E")
        await ctx.send("Suggestion Posted!")

    @suggest.command()
    @commands.has_guild_permissions(manage_guild=True)
    async def channel(self, ctx, channel: typing.Optional[discord.TextChannel]):
        """Changes the suggestion channel (requires manage server permission)"""
        schannel = await self.bot.db.fetch("SELECT * FROM schannel WHERE guild_id = $1 AND channel_id = $2", ctx.guild.id, channel.id)
        if not schannel:
            await self.bot.db.execute("INSERT INTO schannel (guild_id, channel_id) VALUES ($1, $2)", ctx.guild.id, channel.id)
        schannel = await self.bot.db.fetchrow("SELECT * FROM schannel WHERE guild_id = $1 AND channel_id = $2",
                                              ctx.guild.id, channel.id)
        await self.bot.db.execute("UPDATE schannel SET channel_id = $1 WHERE guild_id = $2", schannel["channel_id"],
                                  ctx.guild.id)
        await ctx.send(f"Suggestion channel is now {channel.mention}")


def setup(bot):
    bot.add_cog(Suggestions(bot))