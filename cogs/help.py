import discord
from discord.ext import commands


class EmbedHelpCommand(commands.HelpCommand):
    def __init(self):
        super().__init__(command_attrs=dict(hidden=True))

    def get_ending_note(self):
        return 'Use {0}{1} [category] for more info on a category.'.format(self.clean_prefix, self.invoked_with)

    def get_command_signature(self, command):
        return f'{self.clean_prefix}{command.qualified_name} {command.signature}'

    async def send_bot_help(self, mapping):
        embed = discord.Embed(title='MEE7 Help Menu', description="**Categories**", colour=0xff0000)
        description = self.context.bot.description
        if description:
            embed.description = description

        for cog, commands in mapping.items():
            filtered = await self.filter_commands(commands, sort=True)
            if filtered:
                embed.add_field(name=f"{self.clean_prefix}help {cog.qualified_name.lower()}", value=cog.description,
                                inline=False)

        embed.set_footer(text=self.get_ending_note())
        await self.get_destination().send(embed=embed)

    async def send_cog_help(self, cog):
        embed = discord.Embed(title='{0.qualified_name} Commands'.format(cog), colour=0xff0000)
        commands = []
        filtered = await self.filter_commands(cog.get_commands(), sort=True)
        for command in filtered:
            commands.append(f"`{self.clean_prefix}{command.qualified_name}`")
        description = " ".join(commands)
        embed.description = description
        embed.set_footer(text=f"Use {self.clean_prefix}{self.invoked_with} [command] for more info on a command.")
        await self.get_destination().send(embed=embed)

    async def send_group_help(self, group):
        aliases = group.aliases
        text = " | ".join(aliases)
        if aliases is None:
            text = "No Aliases"
        embed = discord.Embed(title=f"{self.clean_prefix}{group.qualified_name}", colour=0xff0000)
        if group.help:
            embed.description = f"**Aliases:** {text}\n**Description:** {group.help}"

        if isinstance(group, commands.Group):
            filtered = await self.filter_commands(group.commands, sort=True)
            for command in filtered:
                embed.add_field(name=self.get_command_signature(command), value=command.short_doc or '...',
                                inline=False)

        await self.get_destination().send(embed=embed)

    send_command_help = send_group_help


class Help(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.old_help_command = bot.help_command
        bot.help_command = EmbedHelpCommand()
        bot.help_command.cog = self

    def cog_unload(self):
        self.bot.help_command = self.old_help_command


def setup(bot):
    bot.add_cog(Help(bot))
