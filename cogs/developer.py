import discord
from discord.ext import commands


class Dev(commands.Cog, command_attrs=dict(hidden=True)):
    def __init__(self, bot):
        self.bot = bot
        self._last_member = None

    @commands.command()
    async def shutdown(self, ctx):
        """Shutsdown the bot"""
        await ctx.send("Shutting Down!")
        await self.bot.db.close()
        await self.bot.logout()

    @commands.command()
    async def restart(self, ctx, shard: int):
        """Restarts a shard"""
        shard = self.bot.get_shard(shard)
        await shard.restart()
        await ctx.send(f"Successfully restarted shard {shard.id}")


def setup(bot):
    bot.add_cog(Dev(bot))
