import discord
import random
from discord.ext import commands


class Economy(commands.Cog):
    """Fun economy game that everyone can play"""
    def __init__(self, bot):
        self.bot = bot
        self._last_member = None

    @commands.command(aliases=["start"])
    async def register(self, ctx):
        """Registers an account to play"""
        user = await self.bot.db.fetch("SELECT * FROM money WHERE user_id = $1", ctx.author.id)
        if not user:
            await self.bot.db.execute("INSERT INTO money (amount, user_id) VALUES ($1, $2)", 100, ctx.author.id)
            await ctx.send(f"Account registered. Use the balance command to check your balance. Current Balance: 100")
        else:
            await ctx.send("You already have an account")

    @commands.command(aliases=["bal"])
    async def balance(self, ctx):
        """Checks your balance"""
        user = await self.bot.db.fetchrow("SELECT * FROM money WHERE user_id = $1", ctx.author.id)
        if not user:
            await ctx.send("You need to register an account first. Use the register command to start")
        await ctx.send(f"You currently have `{user['amount']}` dollars")

    @commands.command()
    async def gamble(self, ctx, amount: int):
        """Gambles specific amount of money"""
        user = await self.bot.db.fetchrow("SELECT * FROM money WHERE user_id = $1", ctx.author.id)
        if amount > user['amount']:
            await ctx.send("You placed a bet higher than your amount of money")
        else:
            MEE7Outcome = random.randint(1, 10)
            UserOutcome = random.randint(1, 10)
            status = None
            color = None
            if MEE7Outcome > UserOutcome:
                new = (user['amount'] - amount)
                await self.bot.db.execute("INSERT INTO money (amount, user_id) VALUES ($1, $2)", new, ctx.author.id)
                status = 'Lose'
                color = 0xff0000
            elif UserOutcome > MEE7Outcome:
                new = int(user['amount'] + amount)
                await self.bot.db.execute("INSERT INTO money (amount, user_id) VALUES ($1, $2)", new, ctx.author.id)
                status = 'Win'
                color = 0x00ff00
            elif UserOutcome == MEE7Outcome:
                color = 0xffff00
                status = 'Tie. Lost nothing'
            embed = discord.Embed(title=status, description=f"You now have {user['amount']} dollars", color=color)
            embed.add_field(name="MEE7", value=f"Rolled a {MEE7Outcome}")
            embed.add_field(name=ctx.author.name, value=f"Rolled a {UserOutcome}")
            await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Economy(bot))
